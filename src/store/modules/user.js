const state = {
  token: "asd",
  userInfo: { name: "ealien", age: 19 }
};

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token;
  },
  SET_USERINFO: (state, userInfo) => {
    state.userInfo = userInfo;
  }
};

const actions = {
  login({ commit }, userInfo) {
    return new Promise(resolve => {
      commit("SET_USERINFO", userInfo);
      resolve();
    });
  },
  logout({ commit }) {
    return new Promise(resolve => {
      commit("SET_TOKEN", "");
      commit("SET_USERINFO", {});
      resolve();
    });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
