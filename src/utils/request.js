import axios from "axios";
import { getJWT_Token } from "@/utils/auth";
import { Notify } from "vant";

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API
  // timeout: 15000 // 默认无超时
});

// request interceptor
service.interceptors.request.use(
  config => {
    const hasToken = getJWT_Token();
    if (hasToken) {
      config.headers["token"] = getJWT_Token();
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(
  response => {
    const { code } = response.data;
    if (code === 10004) {
      // 身份认证错误时
      // ... do something
      return Promise.reject();
    } else {
      return response;
    }
  },
  error => {
    Notify({
      message: "通知内容",
      duration: 1000,
      background: "#1989fa"
    });
    return Promise.reject(error);
  }
);

export default service;
