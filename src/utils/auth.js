export function setJWT_Token(token) {
  localStorage.setItem("jwt_token", token);
}

export function getJWT_Token() {
  return localStorage.getItem("jwt_token");
}

export function removeJWT_Token() {
  localStorage.removeItem("jwt_token");
}

export function setUserInfo(userInfo) {
  localStorage.setItem("userInfo", JSON.stringify(userInfo));
}

export function getUserInfo() {
  if (!localStorage.getItem("userInfo")) {
    return {};
  } else {
    return JSON.parse(localStorage.getItem("userInfo"));
  }
}

export function removeUserInfo() {
  localStorage.removeItem("userInfo");
}
