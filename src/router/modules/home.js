const homeRouter = [
  {
    path: "/home/details",
    name: "details",
    component: () => import("@/views/home/details/details")
  }
];
export default homeRouter;
