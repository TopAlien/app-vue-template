const tabBarRouter = {
  path: "/",
  redirect: "/home",
  component: () => import("@/views"),
  children: [
    {
      path: "home",
      name: "home",
      component: () => import("@/views/home")
    },
    {
      path: "about",
      name: "about",
      component: () => import("@/views/about")
    },
    {
      path: "community",
      name: "community",
      component: () => import("@/views/community")
    },
    {
      path: "profile",
      name: "profile",
      component: () => import("@/views/profile")
    }
  ]
};
export default tabBarRouter;
