import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

/* Router Modules */
import tabBarRouter from "./modules/tabBar";
import homeRouter from "./modules/home";
import profileRouter from "./modules/profile";

export const constantRoutes = [tabBarRouter, ...homeRouter, ...profileRouter];

const createRouter = () =>
  new Router({
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  });

const router = createRouter();

export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher;
}

export default router;
