# app-template

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod

# 查看生产文件体积
npm run build --report
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
